<?php
$fileList = glob('uploads/*.json');
$test = [];
foreach ($fileList as $key => $file) {
    if ($key == $_GET['test']) {
        $fileTest = file_get_contents($fileList[$key]);
        $decodeFile = json_decode($fileTest, true);
        $test = $decodeFile;
    }
}
$question = $test[0]['question'];
$answers[] = $test[0]['answers'];
// Считаем кол-во правильных ответов
$true = 0;
foreach ($answers[0] as $item) {
    if ($item['result'] === true) {
        $true++;
    }
}
$postTrue = 0;
$postFalse = 0;
if (count($_POST) > 0) {
    // Проверяем и считаем правильность введенных ответов
    foreach ($_POST as $key => $item) {
        if ($answers[0][$key]['result'] === true) {
            $postTrue++;
        }else{
            $postFalse++;
        }
    }
    // Сравниваем и выводим результат
    if ($postTrue === $true && $postFalse === 0) {
        echo 'Правильно!';
    }elseif ($postTrue > 0 && $postFalse > 0) {
        echo 'Почти :)';
    }else{
        echo 'Не верно';
    }
}
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Тест: <?=$question?></title>
</head>
<body>

<form method="post">
    <fieldset>
        <legend><?=$question?></legend>
        <?php foreach ($answers[0] as $key => $item) : ?>
            <label><input type="radio" name="<?=$key;?>" value="<?=$item['answer'];?>"> <?=$item['answer'];?></label>
        <?php endforeach; ?>
    </fieldset>
    <input type="submit" value="Отправить">
</form>

<ul>
    <li><a href="admin.php">Загрузить тест</a></li>
    <li><a href="list.php">Список тестов</a></li>
</ul>

</body>
</html>