
<?php
if (isset($_POST) && isset($_FILES) && isset($_FILES['user_json'])) {
    $uploadName = $_FILES['user_json']['name'];
    $tmpFile = $_FILES['user_json']['tmp_name'];
    $uploadDir = 'uploads/';
    $pathInfo = pathinfo($uploadDir . $uploadName);

    if ($pathInfo['extension'] === 'json') {
        move_uploaded_file($tmpFile, $uploadDir . $uploadName);
        echo 'Ваш тест удачно загружен';

    }else{
        echo 'Ошибка типа файла. Загрузите файл с расширением .json';
    }
}
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Загрузить тест</title>
</head>
<body>

<form method="post" enctype=multipart/form-data>
    <input type=file name=user_json>
    <input type=submit value=Загрузить>
</form>

<ul>
    <li><a href="admin.php">Загрузить тест</a></li>
    <li><a href="list.php">Список тестов</a></li>
</ul>
</body>
</html>